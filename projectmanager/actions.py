from pathlib import Path
from .global_opt import project_dir, projects_file

import os
import git
import json
import shutil
import datetime
import subprocess

class Actions:

    projects = {}

    @staticmethod
    def new_project(name: str, language: str, repo: str = None, explorer: bool = False):
        global project_dir, projects
        Actions.read_proj_file()
        full_dir: str = "{}/{}/{}".format(project_dir, language, name)
        current_time = datetime.datetime.now()
        projects[name] = {
            "language": language,
            "directory": full_dir,
            "created": "{}-{:02}-{:02} {:02}:{:02}".format(current_time.year, current_time.month, current_time.day, current_time.hour, current_time.minute)
        }
        print("\nPreparing project creation...\nCreating project in directory: {}".format(full_dir))
        try:
            os.makedirs(full_dir)
            print("Created project in directory: {}".format(full_dir))
        except FileExistsError:
            print("\nOh no! It looks like that project name is taken!")
            print("Maybe try something else?")
            exit(0)
        repos = None
        if repo:
            projects[name]["git"] = {
                "repository": repo
            }
            print("\nCreating git repository...")
            repos = git.Repo.init(full_dir)
            repos.create_remote("origin", repo)
            print("Created git repository for {}".format(repo))
            print("Adding README.md")
            with open(full_dir + "/README.md", "a") as file_readme:
                file_readme.write("# {}\nDescription Here".format(name))
                print("Added README.md")
                file_readme.close()
        if explorer:
            print("\nOpening in windows explorer...")
            subprocess.Popen(r"explorer {}".format(full_dir.replace("/", "\\")))
            print("Opened directory in windows explorer.")
        print("\nOpening new project in VSCode...")
        os.system("code \"{}\"{}".format(full_dir, "\" " + full_dir + "/README.md\"" if repo else ""))
        print("Opened in VSCode.")
        Actions.write_proj_file()


    @staticmethod
    def delete_project(name: str, language: str = None):
        global project_dir, projects
        if not language:
            print("\nError: Deprecation, you MAY NOT attempt to delete a project without specifying the language.")
            print("You may end up deleting the wrong project by accident.")
            exit(1)
        try:
            projects.pop(name, None)
        except KeyError:
            pass
        full_dir = "{}/{}/{}".format(project_dir, language, name)
        print("\nSearching for project in directory: {}".format(full_dir))
        if Path(full_dir).is_dir():
            print("Found project.")
            print("\nPreparing for termination...")
            shutil.rmtree(full_dir)
            Actions.write_proj_file()
            return
        print("Could not find the project, spelling mistakes?")


    @staticmethod
    def write_proj_file():
        global projects, projects_file
        with open(projects_file, "w+") as proj_file:
            proj_file.write(json.dumps(projects, indent=2, sort_keys=True))
            proj_file.close()


    @staticmethod
    def read_proj_file():
        global projects, projects_file
        if not Path(projects_file).is_file():
            return
        with open(projects_file, "r") as proj_file:
            full_file_str = ""
            for l in [line.strip() for line in proj_file.readlines()]:
                full_file_str += l
            projects = json.loads(full_file_str)
            proj_file.close()
        return projects
