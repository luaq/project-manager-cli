from .global_opt import project_dir, projects_file, projects
from getopt import getopt, GetoptError
from .actions import Actions
from pathlib import Path

import os
import sys
import json
import asyncio


def main(args=None):
    global project_dir, projects_file
    try:
        projs = Actions.read_proj_file()
        context = sys.argv[1]
        if context == "new" or context == "n":
            opts = getopt(sys.argv[2:], "n:l:g:e", ["name=", "lang=", "git=", "explorer"])[0]
            name: str = None
            lang: str = None
            git: str = None
            open_in_explorer: bool = False
            for opt, arg in opts:
                if opt in ("-n", "--name"):
                    name = str(arg)
                elif opt in ("-l", "--lang"):
                    lang = str(arg)
                elif opt in ("-g", "--git"):
                    git = str(arg)
                elif opt in ("-e", "--explorer"):
                    open_in_explorer = True
            if name and lang:
                Actions.new_project(name, lang, git, open_in_explorer)
                print("\nSetup complete. Exiting.")
            else:
                raise ValueError("Missing arguments for context: new")
        elif context == "list" or context == "l":
            if not Path(projects_file).is_file() or len(projs.keys()) == 0:
                print("\nIt appears as though there are no projects created.")
            else:
                print("\nAll your projects:")
                for project_name in projs.keys():
                    project = projs[project_name]
                    print("\n--- {} ---\n".format(project_name))
                    print("Name: {}\nLanguage: {}\nDirectory: {}\nCreated: {}".format(project_name, project["language"], project["directory"], project["created"]))
                    try:
                        print("\nGit Repository: {}".format(project["git"]["repository"]))
                    except KeyError:
                        pass
                    print("\n--- ---")
                print("\nTotal: {}".format(len(projs.keys())))
        elif context == "delete" or context == "remove" or context == "del":
            opts = getopt(sys.argv[2:], "n:l:", ["name=", "lang="])[0]
            name: str = None
            lang: str = None
            for opt, arg in opts:
                if opt in ("-n", "--name"):
                    name = str(arg)
                elif opt in ("-l", "--lang"):
                    lang = str(arg)
            if name:
                try:
                    Actions.delete_project(name, lang)
                    print("\nJob complete. Exiting.")
                except PermissionError:
                    print("\nAn error occured while attempting to delete the project.")
                    print("Check that it's not open in another program.")
            else:
                raise ValueError("Missing arguments for context: delete")
        else:
            raise ValueError("Invalid context: {}".format(context))
    except (GetoptError, ValueError, IndexError) as err:
        if type(err) is ValueError:
            print(err)
            exit(-1)
        send_usage()


def send_usage():
    contexts = ["[n]ew", "[l]ist", "[del]ete, remove"]
    options = ["-n, --name <str>", "-l, --lang <str>", "-g, --git <repos (str)>", "-e, --explorer"]

    print("\nUsage: projectmanager <context> [options]")
    print("\nContexts:")
    for context in contexts:
        print("{:4}{} [options]".format("", context))
    print("\nOptions:")
    for option in options:
        print("{:4}{}".format("", option))


if __name__ == "__main__":
    main()
