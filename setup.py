from setuptools import setup

setup(
    name="projectmanager",
    version="1.0",
    description="Allows developers to manage their projects easier via the commandline.",
    author="luaq.",
    packages=[
        "projectmanager"
    ],
    entry_points={
        "console_scripts": [
            "projectmanager = projectmanager.__main__:main",
            "pm = projectmanager.__main__:main"
        ]
    }
)
