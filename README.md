# Project Manager

This allows developers to easily manage their projects via the command line.

## Installing
**Note: This project was NOT made with all people in mind, and was really meant as a personal project for me. With that being said, this means that some things may need to be changed in the code before usage.** 

You need python3 and pip3 installed, it's not hard to look up how to install those.
Clone this repository into a folder, then open that directory in a terminal. To install, run this command:

    ~$ pip3 install -e .
    
    Installing collected packages: projectmanager
	  Found existing installation: projectmanager 1.0
	Successfully installed projectmanager
With that, you should now be able to use the command globally.

## Using the command
To get help simply type `pm` or `projectmanager` in the command line. Something like this should follow:

    Usage: projectmanager <context> [options]

	Contexts:
	    [n]ew [options]
	    [l]ist [options]
	    [del]ete, remove [options]

	Options:
	    -n, --name <str>
	    -l, --lang <str>
	    -g, --git <repos (str)>
	    -e, --explorer